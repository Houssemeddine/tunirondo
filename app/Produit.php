<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Produit extends Model
{
    protected $table = 'Produit';
    protected $fillable = ['Organisateur','Title','Status','Categorie','DescriptionMineur','Description','PhotosPrincipale','Photos1','Photos2'];

public function Stock()
{
    return $this->hasMany('App\Stock');
}
    public function ssCategory()
    {
        return $this->belongsTo('App\ssCategory');
    }
}
