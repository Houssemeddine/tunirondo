<?php
use Illuminate\Support\Facades\Input ;


use App\Stock ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Panier ;
use App\Commande;
use ZanySoft\LaravelPDF\PDF;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::post('/envoyer', function (Request $request) {

    $data = array('mess' => input::get('message'), 'email' => input::get('email'), 'nom' =>input::get('nom'), 'prenom' =>input::get('prénom'), 'telephone' =>input::get('telephone'));


    Mail::send('email.email', $data, function($message) {
        $message->from('tunisrando@gmail.com', 'Sender Name')->to('tunisrando@gmail.com', 'Receiver Name')->subject('This is the subject');
    });


        return redirect('/contact_us');


});

Route::get('/bb', function() {
    $pdf = PDF::make();
    $pdf->addPage('hello');
    $pdf->send();
});

Route::get('/', function () {
    return view('welcome');
});
Route::get('ajax_produit',function (){
    $date_id = Input::get('date_id');
    $date = Stock::where('id','=',$date_id)->get();
    return Response::json($date);

});


Route::post('ajaxshow', 'FronProduitController@show');

View::composer('welcome', function($view)
{

    $view->with('Blogs', \App\Blogss::all()->take(3));
    $view->with('produits', \App\Produit::all()->take(10));

});
Route::get('error404',[ 'as'=>'notfound','uses'=>'ErrorController@getForm']);

View::composer('layouts.main', function($view)
{
    $view->with('categorie', \App\Category::all()->take(5));
    $view->with('souscategorie', \App\ssCategory::all()->take(4));
    $view->with('menu', \App\menu::all());
    $view->with('detail', \App\Produit::all());
    $view->with('sscategories', \App\ssCategory::all());


    //Si Session panier
    $vieuxPanier = Session::get('panier');
    //On assigne le panier en cours à $panier
    $panier = new Panier($vieuxPanier);
    $view->with('produits', $panier->produits);
    $view->with('prixTotal', $panier->prixTotal);
    $view->with('quantiteTotal', $panier->quantiteTotal);




});

Route::post ( '/Recherche', function () {
$c =Input::get('category_id');
    $q = Input::get ( 'q' );
    $query = $q ;
    if ($c==0)
    $details = \App\Produit::where ( 'Title', 'LIKE', '%' . $q . '%' )->paginate(20);
   else
       $details = \App\Produit::where ( 'Title', 'LIKE', '%' . $q . '%' )->Where ( 'Categorie', '=',  $c  )->paginate(20);

    if (count ( $details ) > 0)

        return view ( 'ProduitRechercher',compact('details','query'));
    else
        return view ( 'ProduitRechercher' )->withMessage ( 'Aucun Produit trouvé. Essayez de rechercher à nouveau!' );
} );


Route::get('/send','SendEmailController@send');



View::composer('shop.panier', function($view)
{
    $view->with('detail', \App\Produit::all());

});
Route::get('grid', 'GridController@getForm');
Route::get('grid/{idss?}',  [
    'uses' => 'GridController@detailssCategorie',
]);
Route::get('detailProduit/{id?}','FrontProduitController@getProduit');


Route::get('creation_compte', 'ContactController@getForm');
Route::post('creation_compte', 'ContactController@postForm');

Route::get('login', 'LoginController@getForm');
Route::post('login', 'LoginController@postForm');

Route::auth();

Route::get('list', 'ListController@getForm');
Route::post('list', 'Listcontroller@postForm');



Route::get('compare', 'CompareController@getForm');
Route::post('compare', 'CompareController@postForm');

Route::get('multiple_addresses', 'Multiple_addressesController@getForm');
Route::post('multiple_addresses', 'Multiple_addressesController@postForm');

Route::get('product_detail', 'Product_detailController@getForm');
Route::post('product_detail', 'Product_detailController@postForm');

Route::get('quick_view', 'Quick_viewController@getForm');
Route::post('quick_view', 'Quick_viewController@postForm');

Route::get('shopping_cart', 'Shopping_cartController@getForm');
Route::post('shopping_cart', 'Shopping_cartController@postForm');

Route::get('forget_password', 'Forget_passwordController@getForm');
Route::post('forget_password', 'Forget_passwordController@postForm');

Route::get('contact_us', 'Contact_usController@getForm');
Route::post('contact_us', 'Contact_usController@postForm');

Route::get('/home', 'HomeController@index');
/*Grid*/


/*Dashboard*/
Route::get('dashboard', 'DashboardController@getForm');
Route::post('dashboard', 'DashboardController@postForm');

/*About_us*/
Route::get('about_us', 'AboutusController@getForm');
Route::post('about_us', 'AboutusController@postForm');

/*Blog*/
Route::get('blog', 'BlogController@getForm');
Route::get('Blog/{id?}', 'BlogController@detailBlog');



/*Chekout*/
Route::get('checkout', 'CheckoutController@getForm');
Route::post('checkout', 'CheckoutController@postForm');

/*Faq*/
Route::get('faq', 'FaqController@getForm');
Route::post('faq', 'FaqController@postForm');

/*Welcome*/
Route::get('welcome', 'IndexController@getForm');
Route::post('welcome', 'IndexController@postForm');

/*Contact_us*/
Route::get('contact_us', 'Contact_usController@getForm');
Route::post('contact_us', 'Contact_usController@postForm');


route::group(['middleware'=>['auth']],function () {

    Route::resource('Profile', 'ProfileController');
    Route::resource('ModifierPassword', 'Auth\ModifierPasswordController');

    route::resource('AjouterProduit', 'FrontAjouteProduitCOntroller');
    route::resource('FrontStock', 'frontStockController');
    Route::resource('ConfirmationFrontProduit', 'confirmationFrontProduitController');
    Route::get('AjouterFrontDate/{id?}', 'AjouterFrontDateController@create');

    Route::resource('AjouterFrontDate', 'AjouterFrontDateController');


    route::resource('Commande', 'FrontCOmmandeController');
    Route::get('facture/{id?}', 'FrontCOmmandeController@facture');




});
route::resource('detailCategorie','FrontssCategorieController');


Route::resource('SendEmail','SendEmailController');

/*backOffice*/


route::get('admin','authadmin\authadminController@showLoginForm');
route::post('Blogin','authadmin\authadminController@login');

route::group(['middleware'=>['admin']],function (){
    route::get('/Bindex','Admin\AdminController@dashboard');
    route::get('/logoutadmin','authadmin\authadminController@logout');
    Route::resource('BBlog','BackBlogController');
    Route::resource('BackCategorie','BackCategorieCOntroller');
    Route::resource('user','BackUserController');
    Route::resource('BProduits','BackProduitController');
    Route::resource('BStock','BackStockController');
    Route::resource('Bprofile','BackProfileController');
    Route::resource('ConfirmationProduit','confirmationProduitController');
    Route::get('AjouterDate/{id?}','AjouterDateController@create');
    Route::get('SupprimerDate/{id?}','AjouterDateController@delete');
    Route::resource('AjouterDate','AjouterDateController');
    Route::resource('ssBackCategorie','ssBackCategorieCOntroller');
    Route::resource('BCommande','BackCommandeController');
    Route::resource('Bmenu','BackMenuController');

    Route::post('BackRechercheMenu','BackMenuController@get_menu_by_recherche');
    Route::post('BackRechercheCategorie','BackCategorieCOntroller@get_Categorie_by_recherche');
    Route::post('BackRecherchessCategorie','ssBackCategorieController@get_ssCategorie_by_recherche');
    Route::post('BackRechercheUtilisateur','BackUserController@get_user_by_recherche');











    route::resource('post','Admin\PostsController');
 
});
route::get('/create',function (){
   App\User::create([
      'name'=>'houssem',
       'lastname'=>'marnissi',
    'role'=>'2',
     'email'=>'admin@admin.com',
        'password'=>bcrypt('09151213'),
   ]);
});
//panier //
Route::post('AjouteProduit',function () {
    $idDate = Input::get ( 'date' );
    $QuantiteSaisie = Input::get ( 'qty' );

    $prixTotaleAvecOption =Input::get ( 'inputPrixTotale' );
   // session_start();
    //$_SESSION['finalPrix']=$prixTotaleAvecOption;4
    session(['finalPrix' => $prixTotaleAvecOption]);
    session(['quantitesaisie' => $QuantiteSaisie]);
    $opt1 = Input::get ( 'Price_1' );
    $opt2 = Input::get ( 'Price_2' );
    $opt3 = Input::get ( 'Price_3' );

    if ($opt1!=null)
        session(['opt1' => $QuantiteSaisie ]);
    else
        session(['opt1' => 0 ]);
if ($opt2!=null)
    session(['opt2' => $QuantiteSaisie ]);
else
    session(['opt2' => 0 ]);
if ($opt3!=null)
    session(['opt3' => $QuantiteSaisie ]);
else
    session(['opt3' => 0 ]);


    return redirect('/ajouter-au-panier/'.$idDate);
}

);
Route::get('/ajouter-au-panier/{id}', [
    'uses' => 'ProduitController@getAjouterAuPanier',
    'as' => 'produit.ajouterAuPanier'
]);
Route::get('/panier', [
    'uses' => 'ProduitController@getPanier',
    'as' => 'produit.panier'
]);
Route::get('/remove-from-cart/{id}', [
    'uses' => 'ProduitController@getRemoveFromCart',
    'as' => 'cart.delete'
]);
Route::get('/paiement', [
    'uses' => 'ProduitController@getPaiement',
    'as' => 'paiement',
    'middleware' => 'auth'
]);
Route::get('/vider-panier', [
    'uses' => 'ProduitController@viderPanier',
    'as' => 'vider.panier'
]);



Route::post('/paiement', [
    'uses' => 'ProduitController@postPaiement',
    'as' => 'paiement',
    'middleware' => 'auth'
]);

Route::get('/acces_non_autorise', function () {
    return view('errors.acces_non_autorise');
})->name('acces_non_autorise');





//Route::group(['middleware' => ['web']], function () {
// //   Route::get('payPremium', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);
    //Route::post('getCheckout', ['as'=>'getCheckout','uses'=>'PaypalController@getCheckout']);
    //Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);
    //Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);
//});




//Route::post('payment', array(
   // 'as' => 'payment',
  //  'uses' => 'IndexController@postPayment',
//));
// this is after make the payment, PayPal redirect back to your site
//Route::get('payment/status', array(
    //'as' => 'payment.status',
    //'uses' => 'IndexController@getPaymentStatus',
//));
Route::get('paywithpaypal', array('as' => 'addmoney.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
Route::post('paypal', array('as' => 'addmoney.paypal','uses' => 'AddMoneyController@postPaymentWithpaypal',));
Route::get('paypal', array('as' => 'payment.status','uses' => 'AddMoneyController@getPaymentStatus',));