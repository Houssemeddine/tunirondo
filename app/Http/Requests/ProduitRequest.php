<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProduitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {$file=config('imagefile');
        return [

            'Organisateur'=>'required|email|max:255',
            'Title'=>'required',
            'Status'=>'required',
            'Categorie'=>'required',

            'DescriptionMineur'=>'required',
            'Description'=>'required',

            'PhotosPrincipale'=>'required',
            'Photos1',
            'Photos2'

        ];
    }
}
