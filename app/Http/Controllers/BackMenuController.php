<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\menu;
use App\Http\Requests;
use App\Http\Requests\MenuRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class BackMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $menu =  DB::table('menu')
            ->paginate(1);
        return view ('BackOffice/Backmenu',compact('menu'));
    }
    public function get_menu_by_recherche()
    {
        $q = Input::get ( 'q' );
        $user = menu::where ( 'id', '=',  $q  )->orWhere ( 'title', 'LIKE', '%' . $q . '%' )->get();
        if (count ( $user ) > 0)
            return view ( 'BackOffice/Recherchemenu' )->withDetails ( $user )->withQuery ( $q );
        else
            return view ( 'BackOffice/Recherchemenu' )->withMessage ( 'Aucunne menu trouvé. Essayez de rechercher à nouveau!' );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('BackOffice.createBackMenu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        menu::create($request->all());
        return redirect('Bmenu')->with('message','Menu ajouter');
    }


    public function edit($id)
    {
        $menu = menu::findOrFail($id);
        return view('BackOffice.editBackMenu',compact('menu'));
    }


    public function update(MenuRequest $request, $id)
    {
        $menu = menu::findOrFail($id);

        $input = $request->all();

        $menu->fill($input)->save();

        return redirect('Bmenu')->with('message','menu  modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = menu::findOrFail($id);
        $menu->delete();
        return redirect('Bmenu')->with('message','menu supprimer');
    }
}
