<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Http\Requests\StockRequest;
use App\Http\Requests;
use App\Produit;
use  DB;



class BackStockController extends Controller
{
    protected $id;
    public function __construct()
    {
        $this->middleware('admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function index()
{
    $idProduit= DB::table( 'Produit')
        ->select('id')->max('id');
    return view('BackOffice.createStockProduit',compact('idProduit'));



}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StockRequest $request)
    {
        $idProduit= DB::table( 'Produit')
            ->select('id')->max('id');
        Stock::create($request->all());

        return redirect ('ConfirmationProduit')->with('message',$idProduit);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function AjouterDate($id)
{
}



    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {$idPRODUIT=DB::table( 'Stock')->where('id', '=', $id)->select('idProduit')->get();
        $stock = Stock::findOrFail($id);
        $stock->delete();
        foreach ($idPRODUIT as $idPRODUIT)
        {
            $idp=$idPRODUIT->idProduit;
        }
        return redirect('BProduits/'.$idp.'/edit')->with('message','date supprimer');
    }
}
