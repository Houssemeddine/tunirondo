<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Http\Requests\StockRequest;
use App\Produit;
use App\Http\Requests;
use DB;

class AjouterDateController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');

    }

    public function index()
    {

     return view('BackOffice.createDate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('BackOffice.createDate',compact('id'));
    }



    public function delete($id)
    {$idp=DB::table( 'Stock')->where('id', '=', $id)->get();
        $stock = Stock::findOrFail($id);
        $stock->delete();
        return redirect('BProduits/'.$idp->idProduit.'/edit')->with('message','date supprimer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StockRequest $request)
    {


Stock::create($request->all());

return redirect ('BProduits/'.$request->idProduit.'/edit')->with('message','Date a été ajouter');

    }


}
