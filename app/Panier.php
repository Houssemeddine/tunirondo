<?php

namespace App;
use Illuminate\Support\Facades\Input ;

use Illuminate\Database\Eloquent\Model;

class Panier extends Model
{
    public $produits = null;
    public $quantiteTotal = 0;
    public $prixTotal = 0;



    public function __construct($vieuxPanier)
    {
        //Si il y avait déjà un panier en cours on set les variables
        if ($vieuxPanier) {
            $this->produits = $vieuxPanier->produits;
            $this->quantiteTotal = $vieuxPanier->quantiteTotal;
            $this->prixTotal = $vieuxPanier->prixTotal;



        }
    }
    //Ensuite on ajoute
    public function ajouter($produit,$id ) {
        $produitPretPourAjout = ['opt1' => 0,'opt2' => 0,'opt3' => 0,'prixAvecOption'=> 0 ,'prixAvecTVA'=>0, 'prix' => 0,'qty' => 0, 'produit' => $produit ];

        //Si il y a un groupe de produits
        if ($this->produits) {
            //Si le produit se trouve dans le groupe de produits
            if (array_key_exists($id, $this->produits)) {
                //Si il est dans le groupe on le selectionne.
                $produitPretPourAjout = $this->produits[$id];
            }
        }
        //On augmente sa quantité, si c'est un nouveau produit entrant dans le groupe alors sa quantité sera de "1" si il était déjà présente alors sa quantité sera de "n +1"
        $produitPretPourAjout['qty']=$produitPretPourAjout['qty']+session('quantitesaisie');
        //Le prix de ce produit sera multiplié par sa quantité
        $produitPretPourAjout['prix'] +=( $produit->Prix0 )* session('quantitesaisie');
        $produitPretPourAjout['prixAvecTVA']+=($produit->Prix0*1.2)  *session('quantitesaisie');
        //Creation du produit dans l'array produitS
        $produitPretPourAjout['prixAvecOption']+= ( session('finalPrix'))* session('quantitesaisie');
        $produitPretPourAjout['opt1']=$produitPretPourAjout['opt1']+session('opt1');
        $produitPretPourAjout['opt2']=$produitPretPourAjout['opt2']+session('opt2');
        $produitPretPourAjout['opt3']=$produitPretPourAjout['opt3']+session('opt3');
        $this->produits[$id] = $produitPretPourAjout;
        //Augmentation de la quantité du panier
        $this->quantiteTotal=$this->quantiteTotal+session('quantitesaisie');
        //Le prix total sera augmenté du prix du produit en cours d'ajout
        $this->prixTotal += session('finalPrix')*session('quantitesaisie') ;

    }
    public function remove($item, $id) {
        //check if cart already has items
        if($this->produits) {
            //if the item exists, remove it from the item array
            if(array_key_exists($id, $this->produits)) {
                $this->quantiteTotal = $this->quantiteTotal - $this->produits[$id]['qty'];
                $this->prixTotal = $this->prixTotal - $this->produits[$id]['prixAvecOption'];
                unset($this->produits[$id]);
            }
            //if there are no more items left, reset total values and unset the items array
            if(count($this->produits) == 0) {
                $this->quantiteTotal = 0;
                $this->prixTotal = 0;
                unset($this->produits);
                return 2;
            }
            return 1;
        }
        else { //if the cart is empty
            return 0;
        }
    }

}
