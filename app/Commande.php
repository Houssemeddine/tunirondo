<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Commande extends Model

{
    protected $table = 'commande';
protected $fillable = ['user_id','facture','montant','payment_id'];
    public function user() {
        return $this->belongsTo('App\User');
    }
}