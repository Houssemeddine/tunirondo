<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class menu extends Model
{
    protected $table = 'menu';
    protected $fillable = ['id','title'];


    public function Category()
    {
        return $this->hasMany('App\Category');
    }
}
