<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'categorie';
    protected $fillable = ['id','title','description','file','idmenu'];

    public function ssCategory() {
        return $this->hasMany('App\ssCategory');
    }
    public function menu() {
        return $this->belongsTo('App\menu');
    }
}
