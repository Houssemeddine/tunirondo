<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Stock extends Model
{
    protected $table = 'Stock';

    protected $fillable = ['idProduit','Date','Stock','Prix0','PrixTVA','Option1','Prix1','Option2','Prix2','Option3','Prix3'];
    public function Produit()
    {
        return $this->belongsTo('App\Produit');
    }

}