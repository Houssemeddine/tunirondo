<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorie', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post_id');

            $table->string('title');
            $table->text('description');
            $table->string('file');
            $table->integer('idmenu')->unsigned();;
            $table->foreign('idmenu')->references('id')->on('menu');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categorie');
    }
}
