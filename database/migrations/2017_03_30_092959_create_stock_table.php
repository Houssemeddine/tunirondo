<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Stock', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('idProduit')->unsigned();
            $table->foreign('idProduit')->references('id')->on('Produit')->onDelete('restrict')->onUpdate('restrict');

            $table->date('Date');
            $table->integer('Stock');
            $table->float('Prix0');

            $table->string('Option1');

            $table->float('Prix1');

            $table->string('Option2');

            $table->float('Prix2');

            $table->string('Option3');
            $table->float('Prix3');


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Stock');



    }
}
