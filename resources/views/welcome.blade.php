@extends('layouts.main')
<!-- Main -->
@section('content')

    <!-- header service -->
    <div id="magik-slideshow" class="magik-slideshow">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-sm-12 col-md-8 wow bounceInUp animated">
                    <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
                        <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                            <ul>
                                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{asset('images/acueil/1.jpg')}}'><img src='{{asset('images/acueil/1.jpg')}}' data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                    <div    class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='45'  data-y='30'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'>Circuit randonnée</div>
                                    <div    class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45'  data-y='70'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'>Tataouine</div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='130'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>Tataouine Prononciation du titre dans sa version originale Écouter
                                        anciennement appelée Foum Tataouine
                                    </div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='400'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;font-size:11px'>C'est une ville du sud-est de la Tunisie située à 531 kilomètres de Tunis</div>
                                </li>
                                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{asset('images/acueil/2.jpg')}}' class="black-text"><img src='{{asset('images/acueil/2.jpg')}}'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='45'  data-y='30'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'>Super Hot</div>
                                    <div    class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45'  data-y='70'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'>Go Lightly</div>
                                    <div    class='tp-caption sfb  tp-resizeme ' data-x='45'  data-y='360'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="view-more">View More</a> <a href='#' class="buy-btn">Buy Now</a></div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='130'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>In augue urna, nunc, tincidunt, augue,<br>
                                        augue facilisis facilisis.</div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='400'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;font-size:11px'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                                </li>
                                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{asset('images/acueil/3.jpg')}}' class="black-text"><img src='{{asset('images/acueil/3.jpg')}}'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='45'  data-y='30'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'>Super Hot</div>
                                    <div    class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45'  data-y='70'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'>Go Lightly</div>
                                    <div    class='tp-caption sfb  tp-resizeme ' data-x='45'  data-y='360'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="view-more">View More</a> <a href='#' class="buy-btn">Buy Now</a></div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='130'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>In augue urna, nunc, tincidunt, augue,<br>
                                        augue facilisis facilisis.</div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='400'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;font-size:11px'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                                </li>
                                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{asset('images/acueil/4.jpg')}}' class="black-text"><img src='{{asset('images/acueil/4.jpg')}}'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='45'  data-y='30'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'>Super Hot</div>
                                    <div    class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45'  data-y='70'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'>Go Lightly</div>
                                    <div    class='tp-caption sfb  tp-resizeme ' data-x='45'  data-y='360'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="view-more">View More</a> <a href='#' class="buy-btn">Buy Now</a></div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='130'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>In augue urna, nunc, tincidunt, augue,<br>
                                        augue facilisis facilisis.</div>
                                    <div    class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='400'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;font-size:11px'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                                </li>
                            </ul>
                            <div class="tp-bannertimer"></div>
                        </div>
                    </div>
                </div>
                <aside class="col-xs-12 col-sm-12 col-md-4 wow bounceInUp animated">
                    <a href="{{url('/detailProduit/7')}}">
                    <div class="RHS-banner">

                        <div class="add"><a href="{{url('/detailProduit/7')}}"><img src="{{asset('images/acueil/wedzitoun.jpg')}}" style="height: 457px;"></a>
                           <a href="{{url('/detailProduit/7')}}"> <div class="overlay"><a class="info" href="{{url('/detailProduit/7')}}">En savoir plus</a></div></a
                        </div>
                    </div></a>
                </aside>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="header-service wow bounceInUp animated">

            <div class="col-lg-3 col-sm-6 col-xs-3">
                <div class="content">
                    <div class="icon-support">&nbsp;</div>
                    <span class="hidden-xs"><strong>Customer Support</strong> Service</span></div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-3">
                <div class="content">
                    <div class="icon-money">&nbsp;</div>
                    <span class="hidden-xs">3 days <strong>Money Back</strong> Guarantee</span></div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-3">
                <div class="content">
                    <div class="icon-dis">&nbsp;</div>
                    <span class="hidden-xs"><strong>5% discount</strong> on order over $199</span></div>
            </div>

        </div>
    </div>
    <section class="latest-blog container wow bounceInUp animated">
        <div class="blog-title">
            <h2><span>Dernier BLOG</span></h2>
        </div>
        @foreach($Blogs as $blog)
        <div class="col-xs-12 col-sm-4">
            <div class="blog-img"> <img src="{{asset($blog->file)}}" alt="Image" width="50" height="200">
                <div class="mask"> <a class="info" href="{{url('Blog/'.$blog->id)}}">En savoir plus</a> </div>
            </div>
            <h2><a href="{{url('Blog/'.$blog->id)}}">{{str_limit($blog->title, $limit = 50, $end = '..')}}</a> </h2>
            <div class="post-date"><i class="icon-calendar"></i> {{$blog->created_at}}</div>
            <p>{{str_limit($blog->description, $limit = 130, $end = '..')}}</p>
        </div>
       @endforeach
    </section>
    <!-- offer banner section -->
    <div class="promo-banner-section container wow bounceInUp animated">
        <div class="row">
            <div class="col-lg-12"><a href="/faq"> <img alt="promo-banner3" src="images/jewelry-banner.jpg"></a></div>
        </div>
    </div>




    <!-- Featured Slider -->
    <section class="featured-pro container wow bounceInUp animated">
        <div class="slider-items-products">
            <div class="new_title center">
                <h2>Derniers produits publiés</h2>
            </div>
            <div id="featured-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col5 ">
@foreach($produits as $produit)
                    <!-- Item -->
                    <div class="item">
                        <div class="col-item">
                            <div class="sale-label sale-top-right">New</div>
                            <div class="product-image-area"> <a class="product-image" title="Sample Product" href="{{url('detailProduit/'.$produit->id)}}"> <img src="{{asset($produit->PhotosPrincipale)}}" height="250" width="220" /> </a>
                            </div>
                            <div class="info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title=" Sample Product" href="{{url('detailProduit/'.$produit->id)}}"> {{$produit->Title}} </a> </div>
                                    <!--item-title-->
                                    <div class="item-content">

                                        <div class="price-box">
                                            <p class="special-price"> <span class="price"> $45.00 </span> </p>

                                        </div>
                                    </div>
                                    <!--item-content-->
                                </div>
                                <!--info-inner-->
                                <div class="actions">
                                   <a href="{{url('detailProduit/'.$produit->id)}}">
                                       <button type="button" title="Ajouter au Panier" class="button btn-cart"><span>ajouter au panier</span></button>
                                    </a>
                                </div>

                                <!--actions-->

                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->



@endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Slider -->
    <!-- promo banner section -->

    <!-- End promo banner section -->
    <!-- middle slider -->
    <section class="middle-slider container wow bounceInUp animated">
        <div class="row">

            <div class="col-md-6">

            </div>
        </div>
    </section>
    <!-- End middle slider -->


@stop
<!-- End Main -->

