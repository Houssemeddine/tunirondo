<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Message de </title>


    <style>	@media only screen and (max-width: 300px){
            body {
                width:218px !important;
                margin:auto !important;
            }
            thead, tbody{width: 100%}
            .table {width:195px !important;margin:auto !important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}
            span.title{font-size:20px !important;line-height: 23px !important}
            span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}
            td.box p{font-size: 12px !important;font-weight: bold !important;}
            .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr {
                display: block !important;
            }
            .table-recap{width: 200px!important;}
            .table-recap tr td, .conf_body td{text-align:center !important;}
            .address{display: block !important;margin-bottom: 10px !important;}
            .space_address{display: none !important;}
        }
        @media only screen and (min-width: 301px) and (max-width: 500px) {
            body {width:425px!important;margin:auto!important;}
            thead, tbody{width: 100%}
            .table {margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
            .table-recap{width: 295px !important;}
            .table-recap tr td, .conf_body td{text-align:center !important;}
            .table-recap tr th{font-size: 10px !important}

        }
        @media only screen and (min-width: 501px) and (max-width: 768px) {
            body {width:478px!important;margin:auto!important;}
            thead, tbody{width: 100%}
            .table {margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
        }
        @media only screen and (max-device-width: 480px) {
            body {width:340px!important;margin:auto!important;}
            thead, tbody{width: 100%}
            .table {margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}

            .table-recap{width: 295px!important;}
            .table-recap tr td, .conf_body td{text-align:center!important;}
            .address{display: block !important;margin-bottom: 10px !important;}
            .space_address{display: none !important;}
        }
    </style>

</head>
<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto" >
<div style="background: #afafaf; height: 100px">
    <center><img src="images/logo.png" style="float: left"></center>
</div>

<table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
    <tr>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        <td align="center" style="padding:7px 0">
            <table class="table" bgcolor="#ffffff" style="width:100%">
                <tr>
                    <td align="center" class="logo" style="border-bottom:4px solid #af93f6;padding:7px 0">

                    </td>
                </tr>

                <tr>
                    <td align="center" class="titleblock" style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <span class="title" style="font-weight:500;font-size:28px;text-transform:uppercase;line-height:33px">Bonjour ,</span><br/>
                            <span class="subtitle" style="font-weight:500;font-size:16px;line-height:25px">Merci d'avoir effectué vos achats sur TuniRando</span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
                        <table class="table" style="width:100%">
                            <tr>
                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                <td style="padding:7px 0">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                        <p data-html-only="1" style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">
                                            Détails de la commande						</p>
                                        <span style="color:#777">
                                            <br></br></span><span style="color:#333"><strong>Paiement avec Paypal </strong></span> <br></br>
							<span style="color:#333"><strong>Commande :</strong></span>  passée le 10/05/2017 à 10:53<br></br>
							<span style="color:#333"><strong>Client :</strong></span><span style="text-transform: uppercase; font-size: 13px"> Houssem Marnissi</span> </span>

                                    </font>
                                </td>
                                <td width="10" style="padding:7px 0">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse"><!-- Title -->
                                <tr>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">Référence</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">Produit</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">Prix unitaire</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">Options</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">Quantité</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">Prix total</th>
                                </tr>
                                <tr>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">56</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">randonné oued zitoun </th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">$15</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">barbaque viande</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">1</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">$28 </th>
                                </tr>
                                <tr>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">10</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">randonné a touzeur </th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">$135</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">boisson gazeux</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">3</th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%">$207 </th>
                                </tr>
                                <tr>
                                    <th bgcolor="#f8f8f8" style="border:  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;"></th>
                                    <th bgcolor="#f8f8f8" style="border:  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;"> </th>
                                    <th bgcolor="#f8f8f8" style="border:  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%"></th>
                                    <th bgcolor="#f8f8f8" style="border:  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%"></th>
                                    <th bgcolor="#f8f8f8" style="border:  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;"></th>
                                    <th bgcolor="#f8f8f8" style="border:1px solid  #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="17%"> $235</th>

                                </tr>
                                </tbody>
                            </table>
                        </font>
                    </td>
                </tr>


                <tr>
                    <td class="footer" style="border-top:4px solid #af93f6;padding:7px 0">
                        <span><a href="" style="color:#337ff1"></a> © 2017. TuniRando.  </a></span>
                    </td>
                </tr>
            </table>
        </td>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
    </tr>
</table>
</body>
</html>