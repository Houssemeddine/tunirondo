<footer class="footer wow bounceInUp animated">
    <div class="brand-logo ">
        <div class="container">
            <div class="slider-items-products">
                <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col6">

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo2.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo3.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo5.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo6.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
                        <!-- End Item -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-7">
                    <div class="block-subscribe">
                        <div class="newsletter">
                            <form>
                                <h4>newsletter</h4>
                                <input type="text" placeholder="Enter your email address" class="input-text required-entry validate-email" title="Sign up for our newsletter" id="newsletter1" name="email">
                                <button class="subscribe" title="Subscribe" type="submit"><span>Subscribe</span></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5">
                    <div class="social">
                        <ul>
                            <li class="fb"><a href="#"></a></li>
                            <li class="tw"><a href="#"></a></li>
                            <li class="googleplus"><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="footer-logo"><a href="/PFE/index" title="Logo"><img src="images/logo.png" alt="logo"></a></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu. </p>
                <div class="payment-accept">
                    <div><img src="images/payment-1.png" alt="payment"> </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4">
                <h4>Shopping Guide</h4>
                <ul class="links">
                    <li class="first"><a href="#" title="How to buy">How to buy</a></li>
                    <li><a href="faq.blade.php" title="FAQs">FAQs</a></li>
                    <li><a href="#" title="Payment">Payment</a></li>
                    <li><a href="#" title="Where is my order?">Where is my order?</a></li>
                    <li class="last"><a href="#" title="Return policy">Return policy</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4">
                <h4>Style Advisor</h4>
                <ul class="links">
                    <li class="first"><a title="Your Account" href="/PFE/login">Your Account</a></li>
                    <li><a title="Information" href="#">Information</a></li>
                    <li><a title="Addresses" href="#">Addresses</a></li>
                    <li><a title="Addresses" href="#">Discount</a></li>
                    <li><a title="Orders History" href="#">Orders History</a></li>
                    <li class="last"><a title=" Additional Information" href="#">Additional Information</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4">
                <h4>Information</h4>
                <ul class="links">
                    <li class="first"><a href="#" title="privacy policy">Privacy policy</a></li>
                    <li><a href="#/" title="Search Terms">Search Terms</a></li>
                    <li><a href="#" title="Advanced Search">Advanced Search</a></li>
                    <li><a href="/PFE/contact_us" title="Contact Us">Contact Us</a></li>
                    <li><a href="#" title="Suppliers">Suppliers</a></li>
                    <li class=" last"><a href="#" title="Our stores" class="link-rss">Our stores</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4">
                <h4>Contact us</h4>
                <div class="contacts-info">
                    <address>
                        <i class="add-icon">&nbsp;</i>Bizertino <br>
                        &nbsp;
                    </address>
                    <div class="phone-footer"><i class="phone-icon">&nbsp;</i> +1 800 123 1234</div>
                    <div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="#">support@magikcommerce.com</a> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-12 coppyright"><center> &copy; 2015. All Rights Reserved. </center> </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
</div>

<!-- JavaScript -->
<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/parallax.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/common.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
</body>
</html>