@extends('layouts.main')



@section('content')
    @if(Session::has('panier'))

        <section class="main-container col1-layout">
            <div class="main container">
                <div class="col-main">
                    <div class="cart wow bounceInUp animated">
                        <div class="page-title">
                            <h2>Panier d'achats</h2>
                        </div>
                        <div class="table-responsive">

                            <input type="hidden" value="Vwww7itR3zQFe86m" name="form_key">
                            <fieldset>
                                <table class="data-table cart-table" id="shopping-cart-table">
                                    <thead>
                                    <tr class="first last">
                                        <th rowspan="1">&nbsp;</th>
                                        <th rowspan="1"><span class="nobr">Nom du produit</span></th>
                                        <th rowspan="1"><span class="nobr">Date</span></th>

                                        <th rowspan="1"><span class="nobr">Prix unitaire</span></th>
                                        <th colspan="1" class="a-center"><span class="nobr">Prix avec TVA</span></th>
                                        <th class="a-center" rowspan="1">Prix Totale (+option)</th>
                                        <th colspan="1" class="a-center">Quantité</th>
                                        <th class="a-center" rowspan="1">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr class="first last">
                                        <td class="a-right last" colspan="8">
                                            <a href="/"> <button  class="button btn-continue" ><span> <span>Continuer vos achats</span></span></button></a>
                                            <a href="/panier"> <button href="/admin"class="button btn-update" type="submit"><span><span>Mise à jour panier</span></span></button></a>
                                            <a href="{{ route('vider.panier') }}"> <button class="button btn-empty" type="submit"><span><span>Vider panier</span></span></button></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($produits as $produit)
                                        <tr class="last even">
                                            @foreach($detail as $details)

                                                @if($details->id == $produit['produit']['idProduit'])

                                            <td class="image"><a class="product-image" title="Sample Product" href="{{url('detailProduit/'.$details->id)}}"><img style="height: 50px;"  width="75" alt="Sample Product" src="{{asset($details->PhotosPrincipale)}}"></a></td>
                                            <td><h2 class="product-name"> <a href="{{url('detailProduit/'.$details->id)}}"> {{$details->Title}}</a> </h2></td>

                                            @endif

                                                    @endforeach
                                                <td class="a-center"> {{ $produit['produit']['Date'] }}</td>

                                                <td class="a-center"> <span class="price">${{  $produit['prix']}}</span></td>
                                            <td class="a-right"><span class="cart-price"> <span class="price">${{ $produit['prixAvecTVA'] }}</span> </span></td>
                                            <td class="a-center movewishlist"><span class="cart-price"> <span class="price">${{ $produit['prixAvecOption'] }}</span> </span></td>
                                            <td class="a-right movewishlist"><span class="cart-price"> <span class="price"><span class="badge">{{ $produit['qty'] }}</span></span> </span></td>
                                            <td class="a-center last"><a class="button remove-item" title="Effacer Produit" href="{{url('remove-from-cart/'.$produit['produit']['id'])}}"><span><span>Effacer Produit</span></span></a></td>
                                        </tr>
                                        <style>
                                            .tableOption th:last-child{
                                                border:1px solid #eaeaea;
                                            }
                                            .tableOption2 td:last-child{
                                                border:1px solid #eaeaea!important;
                                            }

                                            .tableOption th:first-child{
                                                border:1px solid #eaeaea;
                                            }
                                            .tableOption2 td:first-child{
                                                border:1px solid #eaeaea!important;
                                            }
                                        </style>
                                        <tr class="tableOption " style="width: 100px" >

                                            @if($produit['produit']['Option1']!= "")
                                                <th class="a-center" ></th>

                                                <th class="a-center" ><span class="nobr">Option N°1</span></th>

                                            @endif
                                            @if($produit['produit']['Option2']!= "")
                                                <th class="a-center" ><span class="nobr">Option N°2</span></th>

                                            @endif
                                            @if($produit['produit']['Option3']!= "")
                                                <th class="a-center " ><span class="nobr">Option N°3</span></th>

                                            @endif
                                        </tr>
                                        <tr class="tableOption2">
                                            @if($produit['produit']['Option1']!= "")
                                                <th class="a-center" >Nom de l'option</th>
                                                <td class="a-center movewishlist" > {{$produit['produit']['Option1']}}</td>

                                            @endif
                                            @if($produit['produit']['Option2']!= "")
                                                <td class="a-center movewishlist" > {{$produit['produit']['Option2']}}</td>

                                            @endif
                                            @if($produit['produit']['Option3']!= "")
                                                <td class="a-center movewishlist" > {{$produit['produit']['Option3']}}</td>

                                            @endif
                                        </tr>
                                        <tr class="tableOption2">
                                            @if($produit['produit']['Option1']!= "")
                                                <th class="a-center" >Quantité</th>

                                                <td class="a-right movewishlist"><span class="cart-price"> <span class="price"><span class="badge">{{ $produit['opt1'] }}</span></span> </span></td>

                                            @endif
                                            @if($produit['produit']['Option2']!= "")
                                                    <td class="a-right movewishlist"><span class="cart-price"> <span class="price"><span class="badge">{{ $produit['opt2'] }}</span></span> </span></td>

                                            @endif
                                            @if($produit['produit']['Option3']!= "")
                                                    <td class="a-right movewishlist"><span class="cart-price"> <span class="price"><span class="badge">{{ $produit['opt3'] }}</span></span> </span></td>

                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </fieldset>

                        </div>
                    </div>
                    <!-- BEGIN CART COLLATERALS -->
                    <div class="cart-collaterals row  wow bounceInUp animated">

                        <div class="col-sm-4">

                        </div>
                        <div class="col-sm-4">
                            <div class="totals">
                                <h3>Totale d'achats</h3>
                                <div class="inner">
                                    <table id="shopping-cart-totals-table" class="table shopping-cart-table-total">
                                        <colgroup>
                                            <col>
                                            <col width="1">
                                        </colgroup>
                                        <tfoot>
                                        <tr>
                                            <td class="a-left" colspan="1"><strong>Prix Totale</strong></td>
                                            <td class="a-right"><strong><span class="price">$ {{ $prixTotal }}</span></strong></td>

                                        </tr>
                                        </tfoot>

                                    </table>
                                    <ul class="checkout">
                                        <li>
                                            <a href="{{ route('paiement') }}"> <button type="button" title="Proceed to Checkout" class="button btn-proceed-checkout" ><span>Passer au paiement</span></button></a>
                                        </li>


                                    </ul>
                                </div>
                                <!--inner-->
                            </div>
                            <!--totals-->
                        </div>
                        <!--cart-collaterals-->

                    </div>

                </div>
            </div>
        </section>
    @else
        <section class="main-container col1-layout">
            <div class="main container">
                <div class="col-main">
                    <div class="cart wow bounceInUp animated">
                        <div class="page-title">
                            <h2>Panier d'achats</h2>
                        </div>
                        <div class="table-responsive">

                            <input type="hidden" value="Vwww7itR3zQFe86m" name="form_key">
                            <fieldset>
                                <table class="data-table cart-table" id="shopping-cart-table">
                                    <thead>
                                    <tr class="first last">
                                        <th rowspan="1">&nbsp;</th>
                                        <th rowspan="1"><span class="nobr">Nom du produit</span></th>
                                        <th rowspan="1"><span class="nobr">Prix unitaire</span></th>
                                        <th colspan="1" class="a-center"><span class="nobr">Prix avec TVA</span></th>
                                        <th class="a-center" rowspan="1">Prix Totale (+option)</th>
                                        <th colspan="1" class="a-center">Quantité</th>
                                        <th class="a-center" rowspan="1">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody >
                                    <tr class="last even"><td colspan="4"><h3>Aucuns produits dans le panier!</h3></td></tr>
                                    </tbody>
                                    <tfoot>
                                    <tr class="first last">
                                        <td class="a-right last" colspan="7">
                                            <a href="/"> <button  class="button btn-continue" ><span> <span>Continuer vos achats</span></span></button></a>
                                            <a href="/panier"> <button href="/admin"class="button btn-update" type="submit"><span><span>Mise à jour panier</span></span></button></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                            </fieldset>

                        </div>
                    </div>


                </div>
            </div>
        </section>


    @endif

@stop
