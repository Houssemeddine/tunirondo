@extends('layouts.main')
<!-- Main -->
@section('content')



    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-12">
                    <div class="page-title">
                        <h2>Ajouter un évenement</h2>
                    </div>
                    {!! Form::open(array('route'=>'AjouterProduit.store','method'=>'POST', 'files'=>'true')) !!}
                    {{ csrf_field() }}
                    <div class="form-group">


                                <input type="hidden" name="Organisateur" id="Organisateur"value="{{Auth::user()->email}}">

                        </div>



                    <div class="form-group">
                        <div class="form-group ">
                            <label for="select">Categorie</label>

                            <select class="form-control" name="Categorie" value=Categorie>
                                <option value="">Aucunne </option>
                                @foreach($ssCategorie as $ssCategorie)

                                    <option value="{{$ssCategorie->id}}" >{{$ssCategorie->title}}</option>

                                @endforeach
                            </select>

                        </div>
                    </div>

                    <input type="hidden" id="Status" name="Status" value="0" checked />

                    <div class="form-group">
                        {!! Form::label('Titre','Titre') !!}
                        {!! Form::text('Title',null,['class'=>'form-control']) !!}
                        <div>

                            @if ($errors->has('Title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('Title') }}</strong>
                                    </span>
                            @endif</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Description Principale','Description Principale') !!}
                        {!! Form::text('DescriptionMineur',null,['class'=>'form-control']) !!}
                        @if ($errors->has('DescriptionMineur'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('DescriptionMineur') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('Description','Desciption') !!}
                        {!! Form::textarea('Description',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}
                        <div>@if ($errors->has('Description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('Description') }}</strong>
                                    </span>
                            @endif</div>
                    </div>

                    <!-- Example Wizard Accordion -->
                    <div class="margin-bottom-30">
                        <div class="panel-group" id="exampleWizardAccordion" aria-multiselectable="true"
                             role="tablist">
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading1" role="tab">
                                    <a class="panel-title" data-toggle="collapse" href="#exampleCollapse1" data-parent="#exampleWizardAccordion"
                                       aria-expanded="true" aria-controls="exampleCollapse1">
                                        Ajouter  la photos principale
                                    </a>
                                </div>
                                <div class="panel-collapse collapse in" id="exampleCollapse1" aria-labelledby="exampleHeading1"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('PhotosPrincipale',null,['class'=>'form-control']) !!}                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading2" role="tab">
                                    <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse2"
                                       data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse2">
                                        Ajouter une deuxième photos
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapse2" aria-labelledby="exampleHeading2"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('Photos1',null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading3" role="tab">
                                    <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse3"
                                       data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse3">
                                        Ajouter une troisieme photos                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapse3" aria-labelledby="exampleHeading3"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('Photos2',null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Example Wizard Accordion -->

                    @if ($errors->has('PhotosPrincipale'))
                        <span class="help-block">
                                        <strong>Il faut ajouter au moins une photos principale</strong>
                                    </span>
                    @endif
                    <div class="form-group">
                        {!! Form::button('suivant',['type'=>'submit','class'=>'button submit']) !!}
                        {!! Form::close() !!}
                        <a href="{{url('AjouterProduit')}}" > <button class="button submit">Annuler</button></a>
                    </div>











                </div>
            </div>
        </div>
    </div>
    @stop