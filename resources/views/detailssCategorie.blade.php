@extends('layouts.main')
<!-- Main -->
@section('content')

    <!-- Two columns content -->
    <div class="main-container col2-left-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-12 col-sm-push-0 wow bounceInUp animated">
                    <div class="category-description std">
                        <div class="slider-items-products">
                            <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                                <div class="slider-items slider-width-col1">

                                    <!-- Item -->
                                    <div class="item"> <a href="#x"><img alt="" src="{{asset($ssCategories->file)}}" height="300" width="1140"></a>
                                        <div class="cat-img-title cat-bg cat-box">
                                            <h2 class="cat-heading">{{$ssCategories->title}}</h2>
                                            <p>{{$ssCategories->description}}</p>
                                        </div>
                                    </div>
                                    <!-- End Item -->


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category-title">
                        <h1>{{$ssCategories->title}}</h1>
                    </div>
@if($produit->count())
                    <div class="category-products">

                        <div class="toolbar">
                            <div class="sorter">
                                <div class="view-mode">  </div>
                            </div>




                            <div class="pager">
                                <div id="limiter">

                                </div>
                                <div class="pages" style="float: inherit">


                                    <ul class="pagination">
                                       {{$produit->render()}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="products-grid">
                            @foreach($produit as $produits)
                            <li class="item col-lg-3 col-md-5 col-sm-7 col-xs-6">
                                <div class="col-item">

                                    <div class="product-image-area"> <a class="product-image" title="Sample Product" href="{{url('detailProduit/'.$produits->id)}}"> <img src="{{asset($produits->PhotosPrincipale)}}"  alt="a" width="300" height="250"/> </a>
                                        <div class="hover_fly"> <a class="exclusive ajax_add_to_cart_button" href="{{url('ajouter-au-panier/'.$produits->id)}}" title="Add to cart">
                                                <div><i class="icon-shopping-cart"></i><span>Ajouter au panier</span></div>
                                            </a> <a class="quick-view" href="">
                                                <div><i class="icon-eye-open"></i><span>Vue rapide</span></div>

                                            </a> </div>
                                    </div>
                                    <div class="info">
                                        <div class="info-inner">
                                            <div class="item-title"> <a title=" Sample Product" href="{{url('detailProduit/'.$produits->id)}}">{{$produits->Title}}</a> </div>
                                            <!--item-title-->
                                            <div class="item-content">

                                                <div class="price-box">
                                                    <p class="special-price"> <span class="price"> $45.00 </span> </p>

                                                </div>
                                            </div>
                                            <!--item-content-->
                                        </div>
                                        <!--info-inner-->

                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                            </li>
                                @endforeach
                        </ul>
                        <div class="toolbar">
                            <div class="sorter">
                                <div class="view-mode">  </div>
                            </div>

                            <div class="pager">
                                <div id="limiter">

                                </div>
                                <div class="pages" style="float: inherit">


                                    <ul class="pagination">
                                        {{$produit->render()}}
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
  @else

                    <h2>cette categorie contient aucun produit</h2>
   @endif
                </section>

            </div>
        </div>
    </div>
@stop