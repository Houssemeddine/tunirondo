@extends('layouts.main')
<!-- Main -->
@section('content')


    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h2> Créer un compte</h2>
                    </div>
                    <div class="static-contain">
                        <form method="POST" action="{{ url('/register') }}">
                        <fieldset class="group-select">
                            {{ csrf_field() }}

                            <fieldset>
                                <legend>New Address</legend>
                                <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                                <ul>
                                    <li>
                                        <div class="customer-name">
                                            <div class="input-box name-firstname">
                                                <label for="billing:firstname"> Nom<span class="required">*</span></label>
                                                <br>
                                                <input type="text" id="name" name="name" value="" title="First Name" class="input-text ">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-box name-lastname">
                                                <label for="billing:lastname"> Prénon <span class="required">*</span> </label>
                                                <br>
                                                <input type="text" id="lastname" name="lastname" value="" title="Last Name" class="input-text">
                                                @if ($errors->has('lastname'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="customer-name">
                                            <div class="input-box name-firstname">
                                                <label for="billing:firstname"> Adresse Email<span class="required">*</span></label>
                                                <br>
                                                <input type="text" id="email" name="email" title="First Name" class="input-text ">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-box name-lastname">
                                                <label for="billing:lastname">Rôle <span class="required">*</span> </label>
                                                <br>
                                                <select  name="role" id="role" class="input-text ">
                                                    <option value="0" selected >Client</option>
                                                    <option value="1">Organisateur</option>


                                                </select>

                                            </div>
                                        </div>
                                    </li>


                                    <li>
                                        <label for="billing:street1">Créez un mot de passe <span class="required">*</span></label>
                                        <br>
                                        <input type="password"  name="password" id="password"  class="input-text required-entry">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </li>
                                    <li>
                                        <label for="billing:street1">Confirmez votre mot de passe <span class="required">*</span></label>
                                        <br>
                                        <input type="password" name="password_confirmation" id="password_confirmation"  class="input-text required-entry">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                        @endif
                                    </li>


                                </ul>
                            </fieldset>

                            <li>
                                <p class="require"><em class="required">* </em>Champs obligatoire</p>
                                <input type="text" name="hideit" id="hideit" value="">
                                <div class="buttons-set">
                                    <button type="submit" title="Submit" class="button create-account"> <span> inscription </span> </button>
                                </div>
                            </li>
                            </ul>
                        </fieldset>
                        </form>

                    </div>
                </section>

            </div>
        </div>
    </div>>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


@stop