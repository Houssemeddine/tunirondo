@extends('layouts.Backmain')
<!-- Main -->
@section('content')

  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Evenements</h1>
      <div class="page-header-actions">
        <form>
          <div class="input-search input-search-dark">
            <i class="input-search-icon wb-search" aria-hidden="true"></i>
            <input type="text" class="form-control" name="" placeholder="Search...">
          </div>
        </form>
      </div>
    </div>
    <div class="page-content">
      <div class="projects-sort">
        <span class="projects-sort-label">Categorie : </span>
        <div class="inline-block dropdown">
          <span class="dropdown-toggle" id="projects-menu" data-toggle="dropdown" aria-expanded="false"
          role="button">
            Type
            <i class="icon wb-chevron-down-mini" aria-hidden="true"></i>
          </span>
          <ul class="dropdown-menu" aria-labelledby="projects-menu" role="menu">
            <li >
              <a href="/Visiter" role="menuitem" tabindex="">Visiter</a>
            </li>
            <li >
              <a href="/Sortir" role="menuitem" tabindex="">Sortir</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="projects-wrap">
        <ul class="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Feb 22, 2015</div>
              <div>Lorem ipsum</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Mar 10, 2015</div>
              <div>Voluptate pariatur</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Mar 8, 2015</div>
              <div>Laboris Excepteur</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">June 22, 2015</div>
              <div>Consequat ullamco</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Apr 30, 2015</div>
              <div>Eiusmod amet</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">May 12, 2015</div>
              <div>ullamco quis</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">May 25, 2015</div>
              <div>Reprehenderit</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Aug 30, 2015</div>
              <div>Ullamco fugiat</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Oct 8, 2015</div>
              <div>Dolor veniam</div>
            </div>
          </li>
          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="{{asset('BackOffice/assets/photos/placeholder.png')}}">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">View Project</button>
                </figcaption>
              </figure>
              <div class="time pull-right">Oct 18, 2015</div>
              <div>Minim officia</div>
            </div>
          </li>
        </ul>
      </div>

      <nav>
        <ul class="pagination pagination-no-border">
          <li class="disabled">
            <a href="javascript:void(0)" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
          <li><a href="javascript:void(0)">2</a></li>
          <li><a href="javascript:void(0)">3</a></li>
          <li><a href="javascript:void(0)">4</a></li>
          <li><a href="javascript:void(0)">5</a></li>
          <li>
            <a href="javascript:void(0)" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>



@stop