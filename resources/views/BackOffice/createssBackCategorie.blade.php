@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Sous-Categorie</h1>
            <div class="page-header-actions">
                <form>
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="" placeholder="Search...">
                    </div>
                </form>
            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::open(['route'=>'ssBackCategorie.store', 'method'=>'POST', 'files'=>'true']) !!}

                     <div class="form-group">
                         <label for="userfile">Image</label>
                         <input type="file" class="form-control" name="userfile">
                     </div>

                     <div class="form-group">
                         <select class="form-control" name="tag_id" value="tag_id">
                             @foreach($categorie as $Categories)

                                 <option value="{{$Categories->id}}" >{{$Categories->title}}</option>

                             @endforeach
                         </select>
                     </div>

                     <div class="form-group">
                         <label for="title">Title</label>
                         <input type="text" class="form-control" name="title">
                     </div>

                     <div class="form-group">
                         <label for="description">Description</label>
                         <textarea class="form-control" name="description"></textarea>
                     </div>

                     <div class="form-group">
                         <button type="submit" class="btn btn-primary">Upload</button>
                         <a href="{{ url('/ssBackCategorie') }}" class="btn btn-warning">Cancel</a>
                     </div>

                    {!! Form::close() !!}

                 </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>
@stop