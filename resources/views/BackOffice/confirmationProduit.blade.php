@extends('layouts.backmain')
@section('content')
    <div class="page animsition">

        <div class="page-header">
            <h1 class="page-title">Confirmer ou ajouter une autre date </h1> </div>
        <!-- Steps -->
        <div class="steps steps-sm row" data-plugin="matchHeight" data-by-row="true" role="tablist">
            <div class="step col-md-4 done " data-target="#exampleAccount" role="tab">
                <span class="step-number">1</span>
                <div class="step-desc">
                    <span class="step-title">Produit</span>
                    <p>Ajoute produit</p>
                </div>
            </div>

            <div class="step col-md-4 done" data-target="#exampleBilling" role="tab">
                <span class="step-number">2</span>
                <div class="step-desc">
                    <span class="step-title">Les Dates</span>
                    <p>Ajoute déclinaison </p>
                </div>
            </div>

            <div class="step col-md-4 current" data-target="#exampleGetting" role="tab">
                <span class="step-number">3</span>
                <div class="step-desc">
                    <span class="step-title">Confirmation</span>

                </div>
            </div>
        </div>
        <div class="page-content container-fluid">
            <div class="row">

    <table>
    <tr><div class="text-center margin-vertical-30" style="height: 150px">
        <i class="icon wb-check font-size-80" aria-hidden="true"></i>
            <h3><span>Si vous voulez ajouter une autre date </span><a href="/BStock" ><U>cliquer ici</U></a></h3>
    </div></tr>

        <tr><div class="text-center margin-vertical-20" style="height: 50px">
            </div>
    </tr>


     </table>
                <div class="form-group"><div  class="margin-bottom-20"><a href="BProduits">
                        <center><button class="btn btn-block btn-success" style="width: 150px">Confirmer</button></center></a></div>
            </div>

        </div>
        </div>
    </div>
            <!-- End Steps -->

@stop