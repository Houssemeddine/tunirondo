@extends('layouts.Backmain')
<!-- Main -->
@section('content')

  <!-- Page -->
  <div class="page">
    <div class="page-content padding-30 container-fluid">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xlg-6 col-md-12">
          <!-- Panel Predictions -->
          <div class="widget widget-shadow widget-responsive" id="widgetLineareaColor">
         <div class="widget-content widget-radius bg-white">
         <div class="panel-body container-fluid">
          <div class="row">
            <div class="col-md-3">
              <h4>
                <img class="margin-right-10" src="{{asset('BackOffice/assets/images/logo-blue.png')}}"
                alt="...">TunRando</h4>
              <address>
                795 Folsom Ave, Suite 600
                <br> San Francisco, CA, 94107
                <br>
                <abbr title="Mail">E-mail:</abbr>&nbsp;&nbsp;example@google.com
                <br>
                <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(123) 456-7890
                <br>
                <abbr title="Fax">Fax:</abbr>&nbsp;&nbsp;800-692-7753
              </address>
            </div>
            <div class="col-md-3 col-md-offset-6 text-right">
              <h4>Invoice Info</h4>
              <p>
                <a class="font-size-20" href="javascript:void(0)">#5669626</a>
                <br> To:
                <br>
                <span class="font-size-20">Machi</span>
              </p>
              <span>Invoice Date: January 20, 2015</span>
              <br>
              <span>Due Date: January 22, 2015</span>
            </div>
          </div>

          <div class="page-invoice-table table-responsive">
            <table class="table table-hover text-right">
              <thead>
                <tr>
                  <th class="text-center">#</th>
                  <th>Description</th>
				  <th class="text-right">Etat</th>
                  <th class="text-right">Quantity</th>
                  <th class="text-right">Unit Cost</th>
                  <th class="text-right">Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">
                    1
                  </td>
                  <td class="text-left">
                    Server hardware purchase
                  </td>
				  <td>
                    En cours
                  </td>
                  <td>
                    32
                  </td>
                  <td>
                    $75
                  </td>
                  <td>
                    $2152
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    2
                  </td>
                  <td class="text-left">
                    Office furniture purchase
                  </td>
				  <td>
                    En cours
                  </td>
                  <td>
                    15
                  </td>
                  <td>
                    $169
                  </td>
                  <td>
                    $4169
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    3
                  </td>
                  <td class="text-left">
                    Company Anual Dinner Catering
                  </td>
				  <td>
                    En cours
                  </td>
                  <td>
                    69
                  </td>
                  <td>
                    $49
                  </td>
                  <td>
                    $1260
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    4
                  </td>
                  <td class="text-left">
                    Payment for Jan 2015
                  </td>
				  <td>
                    En cours
                  </td>
                  <td>
                    149
                  </td>
                  <td>
                    $12
                  </td>
                  <td>
                    $866
                  </td>
                </tr>
                </tbody>
               </table>
              </div>
             </div>
            </div>
          </div>
          <!-- End Panel Predictions -->
        </div>

        <div class="col-xlg-8 col-md-12">
          <!-- Panel Statistical -->
          <div class="widget widget-shadow" id="widgetStatistical">
            <div class="widget-content widget-radius bg-white">
              <div class="row no-space height-full" data-plugin="matchHeight">
                <div class="col-sm-8 col-xs-12">
                  <div id="widgetJvmap" class="height-full" data-plugin="vectorMap" data-map="ca_lcc_en"></div>
                </div>
                <div class="col-sm-4 col-xs-12 padding-30">
                  <div class="form-group">
                    <div class="input-search input-search-dark">
                      <i class="input-search-icon wb-search" aria-hidden="true"></i>
                      <input type="text" class="form-control" name="" placeholder="Search...">
                    </div>
                  </div>
                  <p class="font-size-20 blue-grey-700">Statistical</p>
                  <p class="blue-grey-400">Status: live</p>
                  <p>
                    <i class="icon wb-map blue-grey-400 margin-right-10" aria-hidden="true"></i>
                    <span>258 Countries, 4835 Cities</span>
                  </p>
                  <ul class="list-unstyled margin-top-20">
                    <li>
                      <p>VISITS</p>
                      <div class="progress progress-xs margin-bottom-25">
                        <div class="progress-bar progress-bar-info bg-blue-600" style="width: 70.3%" aria-valuemax="100"
                        aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                          <span class="sr-only">70.3%</span>
                        </div>
                      </div>
                    </li>
                    <li>
                      <p>TODAY</p>
                      <div class="progress progress-xs margin-bottom-25">
                        <div class="progress-bar progress-bar-info bg-green-600" style="width: 70.3%" aria-valuemax="100"
                        aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                          <span class="sr-only">70.3%</span>
                        </div>
                      </div>
                    </li>
                    <li>
                      <p>WEEK</p>
                      <div class="progress progress-xs margin-bottom-0">
                        <div class="progress-bar progress-bar-info bg-purple-600" style="width: 70.3%"
                        aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3"
                        role="progressbar">
                          <span class="sr-only">70.3%</span>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- End Panel Statistical -->
        </div>

        <div class="col-xlg-4 col-md-12">
          <!-- Panel Earned -->
          
        </div>

      </div>
    </div>
  </div>
  <!-- End Page -->


