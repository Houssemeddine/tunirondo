@extends('layouts.backmain')
@section('content')
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Blog </h1>
      <div class="page-header-actions">
        <form>
          <div class="input-search input-search-dark">
            <i class="input-search-icon wb-search" aria-hidden="true"></i>
            <input type="text" class="form-control" name="" placeholder="Search...">
          </div>
        </form>
      </div>
    </div>
    <div class="page-content">
      <div class="projects-wrap">
        <ul class="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">

          <li>
            <div class="panel">
              <figure class="overlay overlay-hover animation-hover">
                <img class="caption-figure" src="../../../backOffice/assets/photos/placeholder.png">
                <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                  <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-trash" aria-hidden="true"></i></button>
                  </div>
                  <button type="button" class="btn btn-outline btn-default project-button">Afficher Blog</button>
                </figcaption>
              </figure>
              <div class="time pull-right">azze</div>
              <div>azz</div>
            </div>
          </li>

        </ul>
      </div>


      <nav>
        <ul class="pagination pagination-no-border">
          <li class="disabled">
            <a href="javascript:void(0)" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
          <li><a href="javascript:void(0)">2</a></li>
          <li><a href="javascript:void(0)">3</a></li>
          <li><a href="javascript:void(0)">4</a></li>
          <li><a href="javascript:void(0)">5</a></li>
          <li>
            <a href="javascript:void(0)" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <!--add project form -->

  {!! Form::open([ 'route' => 'categorie.store']) !!}

  <button class="site-action btn-raised btn btn-success btn-floating" data-target="#addProjectForm"
          data-toggle="modal" type="button">
    <i class="icon wb-plus" aria-hidden="true"></i>
  </button>

  <div class="modal fade" id="addProjectForm" aria-hidden="true" aria-labelledby="addProjectForm"
       role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Créer une categorie</h4>
        </div>
        <div class="modal-body">
          <form action="#" method="post" role="form">
            <div class="form-group">
              {!! Form::label('title', 'Title:', ['class' => 'control-label margin-bottom-15']) !!}
              {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('description', 'Description:', ['class' => 'control-label margin-bottom-15']) !!}
              {!! Form::textarea('description', null, ['class' => 'maxlength-textarea form-control mb-sm']) !!}
            </div>



          </form>
        </div>
        <div class="modal-footer text-left">
          {!! Form::submit('Créer', ['class' => 'btn btn-primary']) !!}

          {!! Form::close() !!}
          <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Annuler</a>
        </div>
      </div>
    </div>
  </div>


@stop